module.exports = function (eleventyConfig) {
    // eleventyConfig.setUseGitIgnore(false)
  
    // eleventyConfig.addWatchTarget('./_tmp/style.css')
  
    eleventyConfig.addPassthroughCopy('./style.css')
    eleventyConfig.addPassthroughCopy('img')
  }
  