---
layout: page-layout.njk 
title: Schilderwerken
tags: ['service']
icon: | 
    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01" />
    </svg>
intro: "Voor algemeen schilderwerk bent u bij ons aan het juiste adres. Is uw huidig schilderwerk aan vernieuwing toe? Zit de verf los of is ze verkleurd? Hebt u last van schimmel op de muren? Schakel dan de experts van SCHILDERWERKENplus in om uw muren weer om te toveren tot pareltjes; die tevens beschermd zijn tegen de weersomstandigheden, vocht, enzomeer. Geen project is ons te groot of te klein. Verwacht u een kindje en wilt u de kinderkamer voorzien van een leuke blauwe of roze kleur? Hebt u net een kantoorgebouw gekocht en moeten alle muren wit geverfd worden? Schilderwerk kan er in alle soorten en maten voorkomen. Geen enkele opdracht is een probleem: van zeer grote panden tot en met kleine ruimtes."
--- 


