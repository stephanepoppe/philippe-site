'use strict';

module.exports.form = async (event) => {

  let body = JSON.parse(event.body);
  const Joi = require('joi');
  const schema = Joi.object({
    name: Joi.string()
      .required(),
    description: Joi.string()
      .required(),
    phone: Joi.string()
      .required(),
    email: Joi.string()
      .email()
      .required()
  })

  const { error, value } = schema.validate(body);
  if (error) {
    return {
      statusCode: 400,
      body: JSON.stringify(
        {
          message: 'Form is invalid',
          body: value
        },
        null,
        2
      ),
    };
  }

  let message = "";
  message += "Afzender: " + body.name + "\n";
  message += "Email: " + body.email + "\n";
  message += "Telefoon: " + body.phone + "\n";
  message += "Bericht: " + body.description + "\n";

  let context = "";
  const sgMail = require('@sendgrid/mail')
  sgMail.setApiKey(process.env.SENDGRID_API_KEY)
  const msg = {
    to: 'stephanepoppe@gmail.com',
    from: 'stephane@schilder-dewilde.be',
    subject: 'Niew bericht van: ' + body.name,
    text: message,
  }
  
  await sgMail.send(msg)
    .then(() => {
      context = "Email sent"
    })
    .catch((error) => {
      context = error.message;
    })

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Ok',
      },
      {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST, OPTIONS',
        'Access-Control-Allow-Headers': 'Origin, Content-Type',
      },
      2
    ),
  };
};
